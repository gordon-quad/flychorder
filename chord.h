/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _CHORD_H_
#define _CHORD_H_

#include <stdint.h>

#include "ch.h"
#include "hal.h"

#include "keyboard.h"

typedef uint16_t keymap_t;

#define RETURN_CMD        0x01
#define RETURN           (RETURN_CMD << 8)
#define SET_LAYER_CMD     0x02
#define SET_LAYER(x)    ((SET_LAYER_CMD << 8) + x)
#define TOGGLE_LAYER_CMD  0x03
#define TOGGLE_LAYER(x) ((TOGGLE_LAYER_CMD << 8) + x)
#define RESET_CMD         0x04
#define RESET            (RESET_CMD << 8)
#define MRESET_CMD        0x05
#define MRESET           (MRESET_CMD << 8)
#define MACRO_CMD         0x06
#define MACRO(x)        ((MACRO_CMD << 8) + x)
#define MOD_CMD           0x07
#define MOD(x)          ((MOD_CMD << 8) + x)

#define MOD_LeftControl   0x01
#define MOD_LeftShift     0x02
#define MOD_LeftAlt       0x04
#define MOD_LeftGUI       0x08
#define MOD_RightControl  0x10
#define MOD_RightShift    0x20
#define MOD_RightAlt      0x40
#define MOD_RightGUI      0x80

#define BTN_COUNT         7

#define LAYER_STACK_MAX   16

void chord_init(void);

void chord_start(void);
void chord_terminate(void);

extern mailbox_t btn_events;

#endif//_CHORD_H_
