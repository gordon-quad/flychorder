/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <string.h>

#include "ch.h"
#include "hal.h"

#include "btnpad.h"
#include "chord.h"

static uint8_t btnpad_state[BTNPAD_BTN_COUNT];
static uint8_t btnpad_debounce[BTNPAD_BTN_COUNT];
static uint8_t btnpad_raw_state[BTNPAD_BTN_COUNT];

void btnpad_process(int btn, uint8_t val)
{
    if (btnmap[btn] == -1)
        return;
    // If value is calm - increase debounce counter
    if ((val == BTNPAD_INPUT_VALUE_PRESSED) == btnpad_raw_state[btn])
    {
        if (btnpad_debounce[btn] < BTNPAD_DEBOUNCE_COUNT)
            btnpad_debounce[btn]++;
        if ((btnpad_debounce[btn] >= BTNPAD_DEBOUNCE_COUNT) &&
            (btnpad_state[btn] != btnpad_raw_state[btn]))
        {
            btnpad_state[btn] = btnpad_raw_state[btn];

            if (btnpad_state[btn] == 1)
                chMBPostTimeout(&btn_events, btnmap[btn], TIME_IMMEDIATE);
            else
                chMBPostTimeout(&btn_events, 0x100 + btnmap[btn], TIME_IMMEDIATE);
        }
    }
    else
        // Reset it else
        btnpad_debounce[btn] = 0;
    // Write button raw state
    btnpad_raw_state[btn] = (val == BTNPAD_INPUT_VALUE_PRESSED);
}

/*
 * Button pad reader thread
 */
static thread_t *btnpad_thread;
static THD_WORKING_AREA(waBtnpad, 128);
static THD_FUNCTION(BtnpadThread, arg)
{
    (void)arg;
    int btn;
    
    for(;;)
    {
        for (btn = 0; btn < BTNPAD_BTN_COUNT; btn++)
        {
            btnpad_process(btn, palReadPad(btnpad_btns[btn].port,
                                           btnpad_btns[btn].bit));
        }

        if (chThdShouldTerminateX())
            break;

        // Wait a bit before next read
        chThdSleepMilliseconds(BTNPAD_READ_DELAY_MS);
    }
}

/*
 * Button pad init function
 */
void btnpad_init(void)
{
    int btn;

    for (btn = 0; btn < BTNPAD_BTN_COUNT; btn++)
    {
        btnpad_state[btn] = 0;
        btnpad_debounce[btn] = 0;
        btnpad_raw_state[btn] = 0;
        palSetGroupMode(btnpad_btns[btn].port, PAL_PORT_BIT(btnpad_btns[btn].bit), 0,
                        BTNPAD_INPUT_MODE);
    }
}

/*
 * Button pad thread start
 */
void btnpad_start(void)
{
    btnpad_thread = chThdCreateStatic(waBtnpad, sizeof(waBtnpad), NORMALPRIO, BtnpadThread, NULL);
}

void btnpad_terminate(void)
{
    chThdTerminate(btnpad_thread);
    chThdWait(btnpad_thread);
}
