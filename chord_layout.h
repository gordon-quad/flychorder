/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _CHORD_LAYOUT_H_
#define _CHORD_LAYOUT_H_

#include "chord.h"

#define LAYER_COUNT 4

#define MACRO_COUNT 16
#define MACRO_LEN 6

extern const uint16_t * const chords[LAYER_COUNT];
extern const uint16_t * const macros[MACRO_COUNT];
extern const uint16_t layer_activation[LAYER_COUNT];
extern const uint16_t layer_deactivation[LAYER_COUNT];

#endif//_CHORD_LAYOUT_H_
