/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ch.h"
#include "hal.h"
#include "usbcfg.h"

#include "chprintf.h"

#include "bluefruit.h"

#include "chord.h"
#include "btnpad.h"

SerialUSBDriver SDU1;
BaseSequentialStream * chp = (BaseSequentialStream *) &SDU1;

static uint8_t active = false;

int led_blink;

void suspend(void)
{
    palClearPad(IOPORT3, BOARD_LED1);
    SMCR = 0x05; // Power-down + sleep enable
    asm volatile ("sleep" : : : "memory");
    SMCR = 0x00;
}

OSAL_IRQ_HANDLER(WDT_vect)
{
    OSAL_IRQ_PROLOGUE();
    OSAL_IRQ_EPILOGUE();
}

/*
 * Application entry point.
 */
int main(void)
{
    bool conn;
    uint8_t disconn_count = 0;
    /*
     * System initializations.
     * - HAL initialization, this also initializes the configured device drivers
     *   and performs the board-specific initializations.
     * - Kernel initialization, the main() function becomes a thread and the
     *   RTOS is active.
     */
    halInit();
    chSysInit();

    palSetPadMode(IOPORT3, BOARD_LED1, PAL_MODE_OUTPUT_PUSHPULL);
    palClearPad(IOPORT3, BOARD_LED1);

    /*
     * Initializes a serial-over-USB CDC driver.
     */
    sduObjectInit(&SDU1);
    sduStart(&SDU1, &serusbcfg);

    /*
     * Activates the USB driver and then the USB bus pull-up on D+.
     * Note, a delay is inserted in order to not have to disconnect the cable
     * after a reset.
     */
    usbDisconnectBus(serusbcfg.usbp);
    chThdSleepMilliseconds(1000);
    usbStart(serusbcfg.usbp, &usbcfg);
    usbConnectBus(serusbcfg.usbp);

    bluefruit_init();
    bluefruit_keyboard();
    led_blink = 0;
    
    chThdSleepMilliseconds(2000);
    for(;;)
    {
        if (led_blink == 30 && active)
            palSetPad(IOPORT3, BOARD_LED1);
        if (led_blink == 31)
        {
            led_blink = 0;
            if (active)
                palClearPad(IOPORT3, BOARD_LED1);
        }

        conn = bluefruit_connection_status();
        if (conn)
            disconn_count = 0;
        if ((!active) && conn)
        {
            bluefruit_execute_command(AT_HWCONNLED); // turn off stupid led
            btnpad_init();
            chord_init();
            btnpad_start();
            chord_start();
            active = true;
        }
        else if (active && (!conn))
        {
            if (disconn_count < 3)
                disconn_count++;
            {
                btnpad_terminate();
                chord_terminate();
                active = false;
            }
        }

        if (!active)
            suspend();
        else
            chThdSleepMilliseconds(200);

        led_blink++;
    }
}
