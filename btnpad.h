/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _BTNPAD_H_
#define _BTNPAD_H_

#include "ch.h"
#include "hal.h"

enum {
    BTN_P,
    BTN_R,
    BTN_M,
    BTN_I,
    BTN_N,
    BTN_C,
    BTN_F,
};

struct btnpad_matrix_index {
    ioportid_t port;
    uint8_t bit;
};

#define BTNPAD_BTN_COUNT 7

#define BTNPAD_READ_DELAY_MS 2

#define BTNPAD_DEBOUNCE_COUNT 5

#define BTNPAD_INPUT_MODE PAL_MODE_INPUT_PULLUP

#define BTNPAD_INPUT_VALUE_PRESSED 0 // What value means that button is pressed

extern const struct btnpad_matrix_index btnpad_btns[BTNPAD_BTN_COUNT];
extern const int8_t btnmap[BTNPAD_BTN_COUNT];

void btnpad_init(void);
void btnpad_start(void);
void btnpad_terminate(void);

#endif//_BTNPAD_H_
