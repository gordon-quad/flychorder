/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _BLUEFRUIT_H_
#define _BLUEFRUIT_H_

#define BLUEFRUIT_SCK_PORT  IOPORT2
#define BLUEFRUIT_SCK_PIN   PB1
#define BLUEFRUIT_MOSI_PORT IOPORT2
#define BLUEFRUIT_MOSI_PIN  PB2
#define BLUEFRUIT_MISO_PORT IOPORT2
#define BLUEFRUIT_MISO_PIN  PB3

#define BLUEFRUIT_CS_PORT  IOPORT2
#define BLUEFRUIT_CS_PIN   PB4
#define BLUEFRUIT_IRQ_PORT IOPORT5
#define BLUEFRUIT_IRQ_PIN  PE6
#define BLUEFRUIT_RST_PORT IOPORT4
#define BLUEFRUIT_RST_PIN  PD4

#define CMD_INIT   0xBEEF
#define CMD_ATCMD  0x0A00
#define CMD_TX     0x0A01
#define CMD_RX     0x0A02

#define MSG_CMD    0x10
#define MSG_RESP   0x20
#define MSG_ALERT  0x40
#define MSG_ERROR  0x80

#define SDEP_MAX_SIZE    16
#define SDEP_HEADER_SIZE 4

#define SPI_IGNORED_BYTE  0xFE
#define SPI_OVERREAD_BYTE 0xFF

#define CS_DELAY_US 100
#define RST_DELAY_MS 10
#define RESET_DELAY_MS 1000

extern void bluefruit_init(void);
extern void bluefruit_reset(void);
extern bool bluefruit_data_available(void);
extern void bluefruit_send(uint16_t cmd, uint8_t *buf, uint8_t len);
extern bool bluefruit_receive(uint8_t *buf, uint8_t *len, uint8_t *msg_type,
                              uint16_t *cmd_type);
extern void bluefruit_execute_command(uint8_t cmd_index);
extern bool bluefruit_connection_status(void);
extern void bluefruit_send_key(uint8_t key, uint8_t mods);
extern void bluefruit_press_key(uint8_t key, uint8_t mods);
extern void bluefruit_keyboard(void);

#define AT_HWMODELED     0
#define AT_HWCONNLED     1
#define AT_BLEKEYBOARDEN 2
#define AT_BLEPOWERLEVEL 3
#define AT_GAPDEVNAME    4
#define AT_GAPSTARTADV   5
#define AT_GAPSTOPADV    6
#define AT_SOFTRESET     7
#define AT_INFO          8

#define MOD_LEFTCONTROL  0
#define MOD_LEFTSHIFT    1
#define MOD_LEFTALT      2
#define MOD_LEFTGUI      3
#define MOD_RIGHTCONTROL 4
#define MOD_RIGHTSHIFT   5
#define MOD_RIGHTALT     6
#define MOD_RIGHTGUI     7

#endif//_BLUEFRUIT_H_
