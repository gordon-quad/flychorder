/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "chord.h"
#include "btnpad.h"

const struct btnpad_matrix_index btnpad_btns[BTNPAD_BTN_COUNT] =
{
    {IOPORT6, PF7},
    {IOPORT6, PF6},
    {IOPORT6, PF5},
    {IOPORT6, PF4},
    {IOPORT6, PF1},
    {IOPORT6, PF0},
    {IOPORT4, PD7}
};

const int8_t btnmap[BTNPAD_BTN_COUNT] =
{
    BTN_P, BTN_R, BTN_M, BTN_I, BTN_N, BTN_C, BTN_F
};

