/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of Bayan.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>

#include "ch.h"
#include "hal.h"
#include "bluefruit.h"

#include "keyboard.h"

static uint8_t txbuf[SDEP_MAX_SIZE+SDEP_HEADER_SIZE];
static uint8_t rxbuf[SDEP_MAX_SIZE+SDEP_HEADER_SIZE];

static uint8_t respbuf[66];

static char *cmd_send_keys = "AT+BLEKEYBOARDCODE=00-00-00";
static char *cmd_conn_status = "AT+GAPGETCONN";

#define MODS_OFFSET 19
#define KEYS_OFFSET 25

static char *cmds[] = {
    "AT+HWMODELED=0",
    "AT+HWGPIO=19,0",
    "AT+BLEKEYBOARDEN=1",
    "AT+BLEPOWERLEVEL=-20",
    "AT+GAPDEVNAME=FlyChorder",
    "AT+GAPSTARTADV",
    "AT+GAPSTOPADV",
    "ATZ",
    "ATI"
};

extern SerialUSBDriver SDU1;
extern BaseSequentialStream *chp;

static const SPIConfig spiCfg = {
    NULL,                         /* SPI callback.                  */
    BLUEFRUIT_CS_PORT,            /* SPI chip select port.          */
    BLUEFRUIT_CS_PIN,             /* SPI chip select pin.           */
    SPI_CR_DORD_MSB_FIRST     |   /* SPI Data order.                */
    SPI_CR_CPOL_CPHA_MODE(0)  |   /* SPI clock polarity and phase.  */
    SPI_CR_SCK_FOSC_4,            /* SPI clock.                     */
    SPI_SR_SCK_FOSC_2             /* SPI double speed bit.          */
};

void hex_char(uint8_t val, char *res)
{
    if (val < 10)
        *res = '0' + val;
    else
        *res = 'A' + (val-10);
}

void hex(uint8_t val, char *res)
{
    hex_char(val >> 4, res);
    hex_char(val & 0xf, res+1);
}

void bluefruit_init(void)
{
    spiStart(&SPID1, &spiCfg);
    palSetPadMode(BLUEFRUIT_CS_PORT, BLUEFRUIT_CS_PIN, PAL_MODE_OUTPUT_PUSHPULL);
    palSetPadMode(BLUEFRUIT_RST_PORT, BLUEFRUIT_RST_PIN, PAL_MODE_OUTPUT_PUSHPULL);
    palSetPadMode(BLUEFRUIT_SCK_PORT, BLUEFRUIT_SCK_PIN, PAL_MODE_OUTPUT_PUSHPULL);
    palSetPadMode(BLUEFRUIT_MOSI_PORT, BLUEFRUIT_MOSI_PIN, PAL_MODE_OUTPUT_PUSHPULL);
    palSetPadMode(BLUEFRUIT_MISO_PORT, BLUEFRUIT_MISO_PIN, PAL_MODE_INPUT);
    palSetPadMode(BLUEFRUIT_IRQ_PORT, BLUEFRUIT_IRQ_PIN, PAL_MODE_INPUT);
    bluefruit_reset();
}

void bluefruit_reset(void)
{
    palSetPad(BLUEFRUIT_RST_PORT, BLUEFRUIT_RST_PIN);
    palClearPad(BLUEFRUIT_RST_PORT, BLUEFRUIT_RST_PIN);
    chThdSleepMilliseconds(RST_DELAY_MS);
    palSetPad(BLUEFRUIT_RST_PORT, BLUEFRUIT_RST_PIN);
    chThdSleepMilliseconds(RESET_DELAY_MS);
}

bool bluefruit_data_available(void)
{
    if (!palReadPad(BLUEFRUIT_IRQ_PORT, BLUEFRUIT_IRQ_PIN))
        return false;
    return true;
}

void bluefruit_send(uint16_t cmd, uint8_t *buf, uint8_t len)
{
    int ll;
    uint8_t b;

    txbuf[0] = MSG_CMD;
    txbuf[1] = cmd & 0xff;
    txbuf[2] = (cmd >> 8) & 0xff;

    do
    {
        if (len > 16)
        {
            txbuf[3] = 0x90; // len=0x10 + 0x80 more bit
            ll = 16;
            len -= 16;
        }
        else
        {
            txbuf[3] = len;
            ll = len;
            len = 0;
        }

        if (ll > 0)
            memcpy(txbuf+4, buf, ll);
        buf += ll;

        spiSelect(&SPID1);
        spiExchange(&SPID1, 1, txbuf, &b);
        while (b == SPI_IGNORED_BYTE)
        {
            spiUnselect(&SPID1);
            chThdSleepMicroseconds(CS_DELAY_US);
            spiSelect(&SPID1);
            spiExchange(&SPID1, 1, txbuf, &b);
        }

        spiSend(&SPID1, ll + 3, txbuf+1);

        spiUnselect(&SPID1);
    } while(len > 0);
}

bool bluefruit_receive(uint8_t *buf, uint8_t *len, uint8_t *msg_type,
                       uint16_t *cmd_type)
{
    bool more = false;
    uint8_t l = 0;

    if (!bluefruit_data_available())
        return false;

    do {
        spiSelect(&SPID1);

        spiReceive(&SPID1, 1, rxbuf);

        while ((rxbuf[0] == SPI_IGNORED_BYTE) || (rxbuf[0] == SPI_OVERREAD_BYTE)) {
            spiUnselect(&SPID1);
            chThdSleepMicroseconds(CS_DELAY_US);
            spiSelect(&SPID1);
            spiReceive(&SPID1, 1, rxbuf);
        }

        spiReceive(&SPID1, 3, rxbuf+1);

        *msg_type = rxbuf[0];

        if (*msg_type == MSG_ERROR)
        {
            spiUnselect(&SPID1);
            return true;
        }

        *cmd_type = rxbuf[1] + (rxbuf[2] << 8);

        more = (rxbuf[3] & 0x80) != 0;
        rxbuf[3] &= 0x7f;

        if (rxbuf[3] > 16)
        {
            spiUnselect(&SPID1);
            return false;
        }

        if (rxbuf[3] != 0)
            spiReceive(&SPID1, rxbuf[3], rxbuf+4);

        spiUnselect(&SPID1);

        if (rxbuf[3] != 0)
        {
            memcpy(buf+l, rxbuf+4, rxbuf[3]);
            l += rxbuf[3];
        }
    } while(more);

    (*len) = l;
    return true;
}

void bluefruit_execute_command(uint8_t cmd_index)
{
    uint8_t len;
    uint16_t cmd_type;
    uint8_t msg_type;

    spiAcquireBus(&SPID1);
    bluefruit_send(CMD_ATCMD, (uint8_t *)cmds[cmd_index], strlen(cmds[cmd_index]));
    while (!bluefruit_data_available());
    bluefruit_receive(respbuf, &len, &msg_type, &cmd_type);
    spiReleaseBus(&SPID1);
    respbuf[len] = '\0';
}

bool bluefruit_connection_status(void)
{
    uint8_t len;
    uint16_t cmd_type;
    uint8_t msg_type;

    spiAcquireBus(&SPID1);
    bluefruit_send(CMD_ATCMD, (uint8_t *)cmd_conn_status, strlen(cmd_conn_status));
    while (!bluefruit_data_available());
    bluefruit_receive(respbuf, &len, &msg_type, &cmd_type);
    spiReleaseBus(&SPID1);
    respbuf[len] = '\0';

    return respbuf[0] == '1';
}

void bluefruit_send_key(uint8_t key, uint8_t mods)
{
    uint8_t len;
    uint16_t cmd_type;
    uint8_t msg_type;

    hex(mods, cmd_send_keys+MODS_OFFSET);
    hex(key, cmd_send_keys+KEYS_OFFSET);

    bluefruit_send(CMD_ATCMD, (uint8_t *)cmd_send_keys, strlen(cmd_send_keys));
    while (!bluefruit_data_available());
    bluefruit_receive(respbuf, &len, &msg_type, &cmd_type);
    respbuf[len] = '\0';
}

void bluefruit_press_key(uint8_t key, uint8_t mods)
{
    switch (key)
    {
    case KEY_LeftControl:
        mods |= (1<<MOD_LEFTCONTROL);
        break;
    case KEY_LeftShift:
        mods |= (1<<MOD_LEFTSHIFT);
        break;
    case KEY_LeftAlt:
        mods |= (1<<MOD_LEFTALT);
        break;
    case KEY_LeftGUI:
        mods |= (1<<MOD_LEFTGUI);
        break;
    case KEY_RightControl:
        mods |= (1<<MOD_RIGHTCONTROL);
        break;
    case KEY_RightShift:
        mods |= (1<<MOD_RIGHTSHIFT);
        break;
    case KEY_RightAlt:
        mods |= (1<<MOD_RIGHTALT);
        break;
    case KEY_RightGUI:
        mods |= (1<<MOD_RIGHTGUI);
        break;
    }
    spiAcquireBus(&SPID1);
    bluefruit_send_key(key, mods);
    bluefruit_send_key(0x0, 0x0);
    spiReleaseBus(&SPID1);
}


void bluefruit_keyboard(void)
{
    bluefruit_execute_command(AT_HWMODELED);
    bluefruit_execute_command(AT_GAPDEVNAME);
    bluefruit_execute_command(AT_BLEKEYBOARDEN);
    bluefruit_execute_command(AT_SOFTRESET);
}
