/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <avr/pgmspace.h>

#include "chord_layout.h"

const keymap_t layer0[1 << BTN_COUNT] PROGMEM =
{
    0,                              // --- ----  0x00 no keys pressed
    KEY_w_W,                        // --- ---P  0x01
    KEY_y_Y,                        // --- --R-  0x02
    KEY_u_U,                        // --- --RP  0x03
    KEY_r_R,                        // --- -M--  0x04
    0,                              // --- -M-P  0x05
    KEY_h_H,                        // --- -MR-  0x06
    KEY_s_S,                        // --- -MRP  0x07

    KEY_i_I,                        // --- I---  0x08
    KEY_b_B,                        // --- I--P  0x09
    KEY_k_K,                        // --- I-R-  0x0A
    KEY_z_Z,                        // --- I-RP  0x0B
    KEY_d_D,                        // --- IM--  0x0C
    0,                              // --- IM-P  0x0D
    KEY_e_E,                        // --- IMR-  0x0E
    KEY_t_T,                        // --- IMRP  0x0F

    TOGGLE_LAYER(1),                // --N ----  0x10
    TOGGLE_LAYER(2),                // --N ---P  0x11
    KEY_Escape,                     // --N --R-  0x12
    KEY_Semicolon_Colon,            // --N --RP  0x13
    KEY_Comma_LessThan,             // --N -M--  0x14
    0,                              // --N -M-P  0x15
    KEY_Period_GreaterThan,         // --N -MR-  0x16
    MOD(MOD_LeftAlt),               // --N -MRP  0x17

    0,                              // --N I---  0x18
    KEY_Insert,                     // --N I--P  0x19
    0,                              // --N I-R-  0x1A
    MOD(MOD_LeftControl),           // --N I-RP  0x1B
    0,                              // --N IM--  0x1C
    0,                              // --N IM-P  0x1D
    KEY_SingleQuote_DoubleQuote,    // --N IMR-  0x1E
    SET_LAYER(1),                   // --N IMRP  0x1F

    KEY_Spacebar,                   // -C- ----  0x20
    KEY_f_F,                        // -C- ---P  0x21
    KEY_g_G,                        // -C- --R-  0x22
    KEY_v_V,                        // -C- --RP  0x23
    KEY_c_C,                        // -C- -M--  0x24
    KEY_RightBracket_RightBrace,    // -C- -M-P  0x25
    KEY_p_P,                        // -C- -MR-  0x26
    KEY_n_N,                        // -C- -MRP  0x27

    KEY_l_L,                        // -C- I---  0x28
    KEY_x_X,                        // -C- I--P  0x29
    KEY_j_J,                        // -C- I-R-  0x2A
    KEY_q_Q,                        // -C- I-RP  0x2B
    KEY_m_M,                        // -C- IM--  0x2C
    KEY_LeftBracket_LeftBrace,      // -C- IM-P  0x2D
    KEY_a_A,                        // -C- IMR-  0x2E
    KEY_o_O,                        // -C- IMRP  0x2F

    MACRO(0),                       // -CN ----  0x30
    0,                              // -CN ---P  0x31
    0,                              // -CN --R-  0x32
    0,                              // -CN --RP  0x33
    0,                              // -CN -M--  0x34
    0,                              // -CN -M-P  0x35
    0,                              // -CN -MR-  0x36
    0,                              // -CN -MRP  0x37

    MOD(MOD_LeftGUI),               // -CN I---  0x38
    0,                              // -CN I--P  0x39
    0,                              // -CN I-R-  0x3A
    0,                              // -CN I-RP  0x3B
    0,                              // -CN IM--  0x3C
    0,                              // -CN IM-P  0x3D
    KEY_CapsLock,                   // -CN IMR-  0x3E
    KEYPAD_NumLock_Clear,           // -CN IMRP  0x3F

    MOD(MOD_LeftShift),             // F-- ----  0x40
    KEY_ReturnEnter,                // F-- ---P  0x41
    KEY_RightArrow,                 // F-- --R-  0x42
    KEY_DownArrow,                  // F-- --RP  0x43
    KEY_DeleteBackspace,            // F-- -M--  0x44
    KEY_PrintScreen,                // F-- -M-P  0x45
    KEY_DeleteForward,              // F-- -MR-  0x46
    KEY_PageDown,                   // F-- -MRP  0x47

    KEY_LeftArrow,                  // F-- I---  0x48
    KEY_End,                        // F-- I--P  0x49
    KEY_Tab,                        // F-- I-R-  0x4A
    KEY_Home,                       // F-- I-RP  0x4B
    KEY_UpArrow,                    // F-- IM--  0x4C
    KEY_ScrollLock,                 // F-- IM-P  0x4D
    KEY_PageUp,                     // F-- IMR-  0x4E
    SET_LAYER(3),                   // F-- IMRP  0x4F

    KEY_Pause,                      // F-N ----  0x50
    0,                              // F-N ---P  0x51
    0,                              // F-N --R-  0x52
    0,                              // F-N --RP  0x53
    0,                              // F-N -M--  0x54
    0,                              // F-N -M-P  0x55
    0,                              // F-N -MR-  0x56
    0,                              // F-N -MRP  0x57

    0,                              // F-N I---  0x58
    0,                              // F-N I--P  0x59
    0,                              // F-N I-R-  0x5A
    0,                              // F-N I-RP  0x5B
    0,                              // F-N IM--  0x5C
    0,                              // F-N IM-P  0x5D
    0,                              // F-N IMR-  0x5E
    0,                              // F-N IMRP  0x5F

    KEYPAD_Plus,                    // FC- ----  0x60
    KEYPAD_ENTER,                   // FC- ---P  0x61
    KEYPAD_6_RightArrow,            // FC- --R-  0x62
    KEYPAD_2_DownArrow,             // FC- --RP  0x63
    KEYPAD_5,                       // FC- -M--  0x64
    KEYPAD_Asterisk,                // FC- -M-P  0x65
    KEYPAD_Period_Delete,           // FC- -MR-  0x66
    KEYPAD_3_PageDown,              // FC- -MRP  0x67

    KEYPAD_4_LeftArrow,             // FC- I---  0x68
    KEYPAD_1_End,                   // FC- I--P  0x69
    KEYPAD_Minus,                   // FC- I-R-  0x6A
    KEYPAD_7_Home,                  // FC- I-RP  0x6B
    KEYPAD_8_UpArrow,               // FC- IM--  0x6C
    KEYPAD_Slash,                   // FC- IM-P  0x6D
    KEYPAD_9_PageUp,                // FC- IMR-  0x6E
    KEYPAD_0_Insert,                // FC- IMRP  0x6F

    RESET,                          // FCN ----  0x70
    0,                              // FCN ---P  0x71
    0,                              // FCN --R-  0x72
    0,                              // FCN --RP  0x73
    0,                              // FCN -M--  0x74
    0,                              // FCN -M-P  0x75
    0,                              // FCN -MR-  0x76
    0,                              // FCN -MRP  0x77

    0,                              // FCN I---  0x78
    0,                              // FCN I--P  0x79
    0,                              // FCN I-R-  0x7A
    0,                              // FCN I-RP  0x7B
    0,                              // FCN IM--  0x7C
    0,                              // FCN IM-P  0x7D
    0,                              // FCN IMR-  0x7E
    MRESET                          // FCN IMRP  0x7F
};

const keymap_t layer1[1 << BTN_COUNT] PROGMEM =
{
    0,                              // --- ----  0x00
    KEY_5_Percent,                  // --- ---P  0x01
    KEY_4_Dollar,                   // --- --R-  0x02
    MACRO(1),                       // --- --RP  0x03   "" and a back arrow
    KEY_3_Pound,                    // --- -M--  0x04
    0,                              // --- -M-P  0x05
    MACRO(2),                       // --- -MR-  0x06   00
    KEY_Dash_Underscore,            // --- -MRP  0x07

    KEY_2_At,                       // --- I---  0x08
    KEY_Backslash_Pipe,             // --- I--P  0x09
    MACRO(3),                       // --- I-R-  0x0A   $
    KEY_GraveAccent_Tilde,          // --- I-RP  0x0B
    KEY_Slash_Question,             // --- IM--  0x0C
    0,                              // --- IM-P  0x0D
    KEY_Equal_Plus,                 // --- IMR-  0x0E
    MACRO(4),                       // --- IMRP  0x0F   000

    KEY_Spacebar,                   // --N ----  0x10
    TOGGLE_LAYER(2),                // --N ---P  0x11
    KEY_Escape,                     // --N --R-  0x12
    KEY_Semicolon_Colon,            // --N --RP  0x13
    KEY_Comma_LessThan,             // --N -M--  0x14
    0,                              // --N -M-P  0x15
    KEY_Period_GreaterThan,         // --N -MR-  0x16
    MOD(MOD_RightAlt),              // --N -MRP  0x17

    0,                              // --N I---  0x18
    KEY_Insert,                     // --N I--P  0x19
    0,                              // --N I-R-  0x1A
    MOD(MOD_RightControl),          // --N I-RP  0x1B
    0,                              // --N IM--  0x1C
    0,                              // --N IM-P  0x1D
    KEY_SingleQuote_DoubleQuote,    // --N IMR-  0x1E
    RETURN,                         // --N IMRP  0x1F

    KEY_1_Exclamation,              // -C- ----  0x20
    KEY_9_LeftParenthesis,          // -C- ---P  0x21
    KEY_8_Asterisk,                 // -C- --R-  0x22
    KEY_RightBracket_RightBrace,    // -C- --RP  0x23
    KEY_7_Ampersand,                // -C- -M--  0x24
    KEY_RightBracket_RightBrace,    // -C- -M-P  0x25
    MACRO(5),                       // -C- -MR-  0x26   %
    KEY_LeftBracket_LeftBrace,      // -C- -MRP  0x27

    KEY_6_Caret,                    // -C- I---  0x28
    MACRO(6),                       // -C- I--P  0x29   &
    MACRO(7),                       // -C- I-R-  0x2A   () and a back arrow
    MACRO(8),                       // -C- I-RP  0x2B   ?
    MACRO(9),                       // -C- IM--  0x2C   *
    KEY_LeftBracket_LeftBrace,      // -C- IM-P  0x2D
    MACRO(10),                      // -C- IMR-  0x2E   +
    KEY_0_RightParenthesis,         // -C- IMRP  0x2F

    0,                              // -CN ----  0x30
    0,                              // -CN ---P  0x31
    0,                              // -CN --R-  0x32
    0,                              // -CN --RP  0x33
    0,                              // -CN -M--  0x34
    0,                              // -CN -M-P  0x35
    0,                              // -CN -MR-  0x36
    0,                              // -CN -MRP  0x37

    MOD(MOD_LeftGUI),               // -CN I---  0x38
    0,                              // -CN I--P  0x39
    0,                              // -CN I-R-  0x3A
    0,                              // -CN I-RP  0x3B
    0,                              // -CN IM--  0x3C
    0,                              // -CN IM-P  0x3D
    KEY_CapsLock,                   // -CN IMR-  0x3E
    KEYPAD_NumLock_Clear,           // -CN IMRP  0x3F

    MOD(MOD_RightShift),            // F-- ----  0x40
    KEY_ReturnEnter,                // F-- ---P  0x41
    KEY_RightArrow,                 // F-- --R-  0x42
    KEY_DownArrow,                  // F-- --RP  0x43
    KEY_DeleteBackspace,            // F-- -M--  0x44
    KEY_PrintScreen,                // F-- -M-P  0x45
    KEY_DeleteForward,              // F-- -MR-  0x46
    KEY_PageDown,                   // F-- -MRP  0x47

    KEY_LeftArrow,                  // F-- I---  0x48
    KEY_End,                        // F-- I--P  0x49
    KEY_Tab,                        // F-- I-R-  0x4A
    KEY_Home,                       // F-- I-RP  0x4B
    KEY_UpArrow,                    // F-- IM--  0x4C
    KEY_ScrollLock,                 // F-- IM-P  0x4D
    KEY_PageUp,                     // F-- IMR-  0x4E
    0,                              // F-- IMRP  0x4F

    KEY_Pause,                      // F-N ----  0x50
    0,                              // F-N ---P  0x51
    0,                              // F-N --R-  0x52
    0,                              // F-N --RP  0x53
    0,                              // F-N -M--  0x54
    0,                              // F-N -M-P  0x55
    0,                              // F-N -MR-  0x56
    0,                              // F-N -MRP  0x57

    0,                              // F-N I---  0x58
    0,                              // F-N I--P  0x59
    0,                              // F-N I-R-  0x5A
    0,                              // F-N I-RP  0x5B
    0,                              // F-N IM--  0x5C
    0,                              // F-N IM-P  0x5D
    0,                              // F-N IMR-  0x5E
    0,                              // F-N IMRP  0x5F

    KEYPAD_Plus,                    // FC- ----  0x60
    KEYPAD_ENTER,                   // FC- ---P  0x61
    KEYPAD_6_RightArrow,            // FC- --R-  0x62
    KEYPAD_2_DownArrow,             // FC- --RP  0x63
    KEYPAD_5,                       // FC- -M--  0x64
    KEYPAD_Asterisk,                // FC- -M-P  0x65
    KEYPAD_Period_Delete,           // FC- -MR-  0x66
    KEYPAD_3_PageDown,              // FC- -MRP  0x67

    KEYPAD_4_LeftArrow,             // FC- I---  0x68
    KEYPAD_1_End,                   // FC- I--P  0x69
    KEYPAD_Minus,                   // FC- I-R-  0x6A
    KEYPAD_7_Home,                  // FC- I-RP  0x6B
    KEYPAD_8_UpArrow,               // FC- IM--  0x6C
    KEYPAD_Slash,                   // FC- IM-P  0x6D
    KEYPAD_9_PageUp,                // FC- IMR-  0x6E
    KEYPAD_0_Insert,                // FC- IMRP  0x6F

    RESET,                          // FCN ----  0x70
    0,                              // FCN ---P  0x71
    0,                              // FCN --R-  0x72
    0,                              // FCN --RP  0x73
    0,                              // FCN -M--  0x74
    0,                              // FCN -M-P  0x75
    0,                              // FCN -MR-  0x76
    0,                              // FCN -MRP  0x77

    0,                              // FCN I---  0x78
    0,                              // FCN I--P  0x79
    0,                              // FCN I-R-  0x7A
    0,                              // FCN I-RP  0x7B
    0,                              // FCN IM--  0x7C
    0,                              // FCN IM-P  0x7D
    0,                              // FCN IMR-  0x7E
    MRESET                          // FCN IMRP  0x7F
};

const keymap_t layer2[1 << BTN_COUNT] PROGMEM =
{
    0,                              // --- ----  0x00
    KEY_F5,                         // --- ---P  0x01
    KEY_F4,                         // --- --R-  0x02
    0,                              // --- --RP  0x03
    KEY_F3,                         // --- -M--  0x04
    0,                              // --- -M-P  0x05
    0,                              // --- -MR-  0x06
    0,                              // --- -MRP  0x07

    KEY_F2,                         // --- I---  0x08
    0,                              // --- I--P  0x09
    0,                              // --- I-R-  0x0A
    0,                              // --- I-RP  0x0B
    0,                              // --- IM--  0x0C
    0,                              // --- IM-P  0x0D
    0,                              // --- IMR-  0x0E
    0,                              // --- IMRP  0x0F

    0,                              // --N ----  0x10
    0,                              // --N ---P  0x11
    0,                              // --N --R-  0x12
    0,                              // --N --RP  0x13
    0,                              // --N -M--  0x14
    0,                              // --N -M-P  0x15
    0,                              // --N -MR-  0x16
    MOD(MOD_LeftAlt),               // --N -MRP  0x17

    0,                              // --N I---  0x18
    0,                              // --N I--P  0x19
    0,                              // --N I-R-  0x1A
    MOD(MOD_LeftControl),           // --N I-RP  0x1B
    0,                              // --N IM--  0x1C
    0,                              // --N IM-P  0x1D
    0,                              // --N IMR-  0x1E
    0,                              // --N IMRP  0x1F

    KEY_F1,                         // -C- ----  0x20
    KEY_F9,                         // -C- ---P  0x21
    KEY_F8,                         // -C- --R-  0x22
    KEY_F12,                        // -C- --RP  0x23
    KEY_F7,                         // -C- -M--  0x24
    0,                              // -C- -M-P  0x25
    KEY_F11,                        // -C- -MR-  0x26
    0,                              // -C- -MRP  0x27

    KEY_F6,                         // -C- I---  0x28
    0,                              // -C- I--P  0x29
    0,                              // -C- I-R-  0x2A
    0,                              // -C- I-RP  0x2B
    KEY_F10,                           // -C- IM--  0x2C
    0,                              // -C- IM-P  0x2D
    0,                              // -C- IMR-  0x2E
    0,                              // -C- IMRP  0x2F

    0,                              // -CN ----  0x30
    0,                              // -CN ---P  0x31
    0,                              // -CN --R-  0x32
    0,                              // -CN --RP  0x33
    0,                              // -CN -M--  0x34
    0,                              // -CN -M-P  0x35
    0,                              // -CN -MR-  0x36
    0,                              // -CN -MRP  0x37

    0,                              // -CN I---  0x38
    0,                              // -CN I--P  0x39
    0,                              // -CN I-R-  0x3A
    0,                              // -CN I-RP  0x3B
    0,                              // -CN IM--  0x3C
    0,                              // -CN IM-P  0x3D
    0,                              // -CN IMR-  0x3E
    0,                              // -CN IMRP  0x3F

    MOD(MOD_LeftShift),             // F-- ----  0x40
    0,                              // F-- ---P  0x41
    0,                              // F-- --R-  0x42
    0,                              // F-- --RP  0x43
    0,                              // F-- -M--  0x44
    0,                              // F-- -M-P  0x45
    0,                              // F-- -MR-  0x46
    0,                              // F-- -MRP  0x47

    0,                              // F-- I---  0x48
    0,                              // F-- I--P  0x49
    0,                              // F-- I-R-  0x4A
    0,                              // F-- I-RP  0x4B
    0,                              // F-- IM--  0x4C
    0,                              // F-- IM-P  0x4D
    0,                              // F-- IMR-  0x4E
    0,                              // F-- IMRP  0x4F

    0,                              // F-N ----  0x50
    0,                              // F-N ---P  0x51
    0,                              // F-N --R-  0x52
    0,                              // F-N --RP  0x53
    0,                              // F-N -M--  0x54
    0,                              // F-N -M-P  0x55
    0,                              // F-N -MR-  0x56
    0,                              // F-N -MRP  0x57

    0,                              // F-N I---  0x58
    0,                              // F-N I--P  0x59
    0,                              // F-N I-R-  0x5A
    0,                              // F-N I-RP  0x5B
    0,                              // F-N IM--  0x5C
    0,                              // F-N IM-P  0x5D
    0,                              // F-N IMR-  0x5E
    0,                              // F-N IMRP  0x5F

    0,                              // FC-  ---  0x60
    0,                              // FC- ---P  0x61
    0,                              // FC- --R-  0x62
    0,                              // FC- --RP  0x63
    0,                              // FC- -M--  0x64
    0,                              // FC- -M-P  0x65
    0,                              // FC- -MR-  0x66
    0,                              // FC- -MRP  0x67

    0,                              // FC- I---  0x68
    0,                              // FC- I--P  0x69
    0,                              // FC- I-R-  0x6A
    0,                              // FC- I-RP  0x6B
    0,                              // FC- IM--  0x6C
    0,                              // FC- IM-P  0x6D
    0,                              // FC- IMR-  0x6E
    0,                              // FC- IMRP  0x6F

    RESET,                          // FCN ----  0x70
    0,                              // FCN ---P  0x71
    0,                              // FCN --R-  0x72
    0,                              // FCN --RP  0x73
    0,                              // FCN -M--  0x74
    0,                              // FCN -M-P  0x75
    0,                              // FCN -MR-  0x76
    0,                              // FCN -MRP  0x77

    0,                              // FCN I---  0x78
    0,                              // FCN I--P  0x79
    0,                              // FCN I-R-  0x7A
    0,                              // FCN I-RP  0x7B
    0,                              // FCN IM--  0x7C
    0,                              // FCN IM-P  0x7D
    0,                              // FCN IMR-  0x7E
    MRESET                          // FCN IMRP  0x7F
};

const keymap_t layer3[1 << BTN_COUNT] PROGMEM =
{
    0,                              // --- ----  0x00 no keys pressed
    KEY_d_D,                        // --- ---P  0x01 в
    KEY_z_Z,                        // --- --R-  0x02 я
    KEY_e_E,                        // --- --RP  0x03 у
    KEY_h_H,                        // --- -M--  0x04 р
    KEY_o_O,                        // --- -M-P  0x05 щ
    KEY_LeftBracket_LeftBrace,      // --- -MR-  0x06 х
    KEY_c_C,                        // --- -MRP  0x07 с

    KEY_b_B,                        // --- I---  0x08 и
    KEY_Comma_LessThan,             // --- I--P  0x09 б
    KEY_r_R,                        // --- I-R-  0x0A к
    KEY_p_P,                        // --- I-RP  0x0B з
    KEY_l_L,                        // --- IM--  0x0C д
    KEY_Period_GreaterThan,         // --- IM-P  0x0D ю
    KEY_t_T,                        // --- IMR-  0x0E е
    KEY_n_N,                        // --- IMRP  0x0F т

    TOGGLE_LAYER(1),                // --N ----  0x10
    TOGGLE_LAYER(2),                // --N ---P  0x11
    KEY_Escape,                     // --N --R-  0x12
    MACRO(11),                      // --N --RP  0x13 ;
    MACRO(12),                      // --N -M--  0x14 ,
    KEY_RightBracket_RightBrace,    // --N -M-P  0x15 ъ
    MACRO(13),                      // --N -MR-  0x16 .
    MOD(MOD_LeftAlt),               // --N -MRP  0x17

    KEY_m_M,                        // --N I---  0x18 ь
    KEY_Insert,                     // --N I--P  0x19
    KEY_GraveAccent_Tilde,          // --N I-R-  0x1A ё
    MOD(MOD_LeftControl),           // --N I-RP  0x1B
    0,                              // --N IM--  0x1C
    0,                              // --N IM-P  0x1D
    MACRO(14),                      // --N IMR-  0x1E '
    SET_LAYER(1),                   // --N IMRP  0x1F

    KEY_Spacebar,                   // -C- ----  0x20
    KEY_a_A,                        // -C- ---P  0x21 ф
    KEY_u_U,                        // -C- --R-  0x22 г
    KEY_s_S,                        // -C- --RP  0x23 ы
    KEY_w_W,                        // -C- -M--  0x24 ц
    KEY_i_I,                        // -C- -M-P  0x25 ш
    KEY_g_G,                        // -C- -MR-  0x26 п
    KEY_y_Y,                        // -C- -MRP  0x27 н

    KEY_k_K,                        // -C- I---  0x28 л
    KEY_x_X,                        // -C- I--P  0x29 ч
    KEY_q_Q,                        // -C- I-R-  0x2A й
    KEY_Semicolon_Colon,            // -C- I-RP  0x2B ж
    KEY_v_V,                        // -C- IM--  0x2C м
    KEY_SingleQuote_DoubleQuote,    // -C- IM-P  0x2D э
    KEY_f_F,                        // -C- IMR-  0x2E а
    KEY_j_J,                        // -C- IMRP  0x2F о

    MACRO(0),                       // -CN ----  0x30
    0,                              // -CN ---P  0x31
    0,                              // -CN --R-  0x32
    0,                              // -CN --RP  0x33
    0,                              // -CN -M--  0x34
    0,                              // -CN -M-P  0x35
    0,                              // -CN -MR-  0x36
    0,                              // -CN -MRP  0x37

    0,                              // -CN I---  0x38
    0,                              // -CN I--P  0x39
    0,                              // -CN I-R-  0x3A
    0,                              // -CN I-RP  0x3B
    0,                              // -CN IM--  0x3C
    0,                              // -CN IM-P  0x3D
    KEY_CapsLock,                   // -CN IMR-  0x3E
    KEYPAD_NumLock_Clear,           // -CN IMRP  0x3F

    MOD(MOD_LeftShift),             // F-- ----  0x40
    KEY_ReturnEnter,                // F-- ---P  0x41
    KEY_RightArrow,                 // F-- --R-  0x42
    KEY_DownArrow,                  // F-- --RP  0x43
    KEY_DeleteBackspace,            // F-- -M--  0x44
    KEY_PrintScreen,                // F-- -M-P  0x45
    KEY_DeleteForward,              // F-- -MR-  0x46
    KEY_PageDown,                   // F-- -MRP  0x47

    KEY_LeftArrow,                  // F-- I---  0x48
    KEY_End,                        // F-- I--P  0x49
    KEY_Tab,                        // F-- I-R-  0x4A
    KEY_Home,                       // F-- I-RP  0x4B
    KEY_UpArrow,                    // F-- IM--  0x4C
    KEY_ScrollLock,                 // F-- IM-P  0x4D
    KEY_PageUp,                     // F-- IMR-  0x4E
    SET_LAYER(0),                   // F-- IMRP  0x4F

    KEY_Pause,                      // F-N ----  0x50
    0,                              // F-N ---P  0x51
    0,                              // F-N --R-  0x52
    0,                              // F-N --RP  0x53
    0,                              // F-N -M--  0x54
    0,                              // F-N -M-P  0x55
    0,                              // F-N -MR-  0x56
    0,                              // F-N -MRP  0x57

    0,                              // F-N I---  0x58
    0,                              // F-N I--P  0x59
    0,                              // F-N I-R-  0x5A
    0,                              // F-N I-RP  0x5B
    0,                              // F-N IM--  0x5C
    0,                              // F-N IM-P  0x5D
    0,                              // F-N IMR-  0x5E
    0,                              // F-N IMRP  0x5F

    KEYPAD_Plus,                    // FC- ----  0x60
    KEYPAD_ENTER,                   // FC- ---P  0x61
    KEYPAD_6_RightArrow,            // FC- --R-  0x62
    KEYPAD_2_DownArrow,             // FC- --RP  0x63
    KEYPAD_5,                       // FC- -M--  0x64
    KEYPAD_Asterisk,                // FC- -M-P  0x65
    KEYPAD_Period_Delete,           // FC- -MR-  0x66
    KEYPAD_3_PageDown,              // FC- -MRP  0x67

    KEYPAD_4_LeftArrow,             // FC- I---  0x68
    KEYPAD_1_End,                   // FC- I--P  0x69
    KEYPAD_Minus,                   // FC- I-R-  0x6A
    KEYPAD_7_Home,                  // FC- I-RP  0x6B
    KEYPAD_8_UpArrow,               // FC- IM--  0x6C
    KEYPAD_Slash,                   // FC- IM-P  0x6D
    KEYPAD_9_PageUp,                // FC- IMR-  0x6E
    KEYPAD_0_Insert,                // FC- IMRP  0x6F

    RESET,                          // FCN ----  0x70
    0,                              // FCN ---P  0x71
    0,                              // FCN --R-  0x72
    0,                              // FCN --RP  0x73
    0,                              // FCN -M--  0x74
    0,                              // FCN -M-P  0x75
    0,                              // FCN -MR-  0x76
    0,                              // FCN -MRP  0x77

    0,                              // FCN I---  0x78
    0,                              // FCN I--P  0x79
    0,                              // FCN I-R-  0x7A
    0,                              // FCN I-RP  0x7B
    0,                              // FCN IM--  0x7C
    0,                              // FCN IM-P  0x7D
    0,                              // FCN IMR-  0x7E
    MRESET                          // FCN IMRP  0x7F
};

const keymap_t * const chords[LAYER_COUNT] PROGMEM =
{
    layer0,
    layer1,
    layer2,
    layer3
};

const uint16_t macro0[MACRO_LEN] PROGMEM  = {TOGGLE_LAYER(1),        MOD(MOD_LeftShift),          0,                      0,                           0,             0};
const uint16_t macro1[MACRO_LEN] PROGMEM  = {MOD(MOD_LeftShift),     KEY_SingleQuote_DoubleQuote, MOD(MOD_LeftShift),     KEY_SingleQuote_DoubleQuote, KEY_LeftArrow, 0};
const uint16_t macro2[MACRO_LEN] PROGMEM  = {KEY_0_RightParenthesis, KEY_0_RightParenthesis,      0,                      0,                           0,             0};
const uint16_t macro3[MACRO_LEN] PROGMEM  = {MOD(MOD_LeftShift),     KEY_4_Dollar,                0,                      0,                           0,             0};
const uint16_t macro4[MACRO_LEN] PROGMEM  = {KEY_0_RightParenthesis, KEY_0_RightParenthesis,      KEY_0_RightParenthesis, 0,                           0,             0};
const uint16_t macro5[MACRO_LEN] PROGMEM  = {MOD(MOD_LeftShift),     KEY_5_Percent,               0,                      0,                           0,             0};
const uint16_t macro6[MACRO_LEN] PROGMEM  = {MOD(MOD_LeftShift),     KEY_7_Ampersand,             0,                      0,                           0,             0};
const uint16_t macro7[MACRO_LEN] PROGMEM  = {MOD(MOD_LeftShift),     KEY_9_LeftParenthesis,       MOD(MOD_LeftShift),     KEY_0_RightParenthesis,      KEY_LeftArrow, 0};
const uint16_t macro8[MACRO_LEN] PROGMEM  = {MOD(MOD_LeftShift),     KEY_Slash_Question,          0,                      0,                           0,             0};
const uint16_t macro9[MACRO_LEN] PROGMEM  = {MOD(MOD_LeftShift),     KEY_8_Asterisk,              0,                      0,                           0,             0};
const uint16_t macro10[MACRO_LEN] PROGMEM = {MOD(MOD_LeftShift),     KEY_Equal_Plus,              0,                      0,                           0,             0};
const uint16_t macro11[MACRO_LEN] PROGMEM = {MOD(MOD_RightAlt),      KEY_Semicolon_Colon,         MOD(MOD_RightAlt),      MOD(MOD_RightAlt),           0,             0};
const uint16_t macro12[MACRO_LEN] PROGMEM = {MOD(MOD_RightAlt),      KEY_Comma_LessThan,          MOD(MOD_RightAlt),      MOD(MOD_RightAlt),           0,             0};
const uint16_t macro13[MACRO_LEN] PROGMEM = {MOD(MOD_RightAlt),      KEY_Period_GreaterThan,      MOD(MOD_RightAlt),      MOD(MOD_RightAlt),           0,             0};
const uint16_t macro14[MACRO_LEN] PROGMEM = {MOD(MOD_RightAlt),      KEY_SingleQuote_DoubleQuote, MOD(MOD_RightAlt),      MOD(MOD_RightAlt),           0,             0};
const uint16_t macro15[MACRO_LEN] PROGMEM = {MOD(MOD_RightAlt),      MOD(MOD_RightAlt),           0,                      0,                           0,             0};

const uint16_t * const macros[MACRO_COUNT] PROGMEM =
{
    macro0, macro1, macro2, macro3, macro4, macro5, macro6, macro7,
    macro8, macro9, macro10, macro11, macro12, macro13, macro14, macro15
};

const uint16_t layer_activation[LAYER_COUNT] = {0, 0, 0, MACRO(15)};
const uint16_t layer_deactivation[LAYER_COUNT] = {0, 0, 0, MOD(MOD_RightAlt)};

