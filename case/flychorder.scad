key_size = 7*2.54;
key_hole = 14;

key_mount_size = 14.4;
key_holder_pin_width = 1.05;

keycap_h = 17;
keycap_w = 17;

smooth_radius = 1.6;

$fn = 50;

module key_mount() {
    difference() {
        intersection() {
            square(key_mount_size, center=true);
            rotate([0,0,45]) square(key_mount_size, center=true);
        }
            rotate([0,0,-45]) translate([-key_holder_pin_width/2,14.4/2-1.5]) square([key_holder_pin_width, 4]);
            rotate([0,0,-135]) translate([-key_holder_pin_width/2,14.4/2-1.5]) square([key_holder_pin_width, 4]);
            rotate([0,0,-225]) translate([-key_holder_pin_width/2,14.4/2-1.5]) square([key_holder_pin_width, 4]);
            rotate([0,0,45]) translate([-key_holder_pin_width/2,14.4/2-1.5]) square([key_holder_pin_width, 4]);
    }
}

module keycap(w_mount) {
    translate([-keycap_w/2, -keycap_h/2]) difference() {
        translate([keycap_w/2, keycap_w/2]) circle(d=keycap_w);
        //translate([smooth_radius, smooth_radius]) minkowski() {
        //    square([keycap_w-2*smooth_radius, keycap_h-2*smooth_radius]);
        //    circle(smooth_radius);
        //}
        if (w_mount)
            translate([keycap_w/2, keycap_h/2]) key_mount();
    }
}

// construction part

pin_height = 2.54 + 8.50;
module feather() {
    height = 50.8;
    width = 22.86;
    radius = 3;
    screw_off = 2.54;
    screw_dia = 2.54;
    right_off_h = 16.51;
    left_off_h = 6.35;
    pin_off_w = 1.27;
    pin_dia = 0.8;
    pin_off = 2.54;

    difference() {
        translate([radius,radius]) minkowski() {
            square([width-radius*2, height-radius*2]);
            circle(r=radius);
        }
        translate([screw_off, screw_off]) circle(d=screw_dia);
        translate([screw_off, height-screw_off]) circle(d=screw_dia);
        translate([width-screw_off, screw_off]) circle(d=screw_dia);
        translate([width-screw_off, height-screw_off]) circle(d=screw_dia);

        for (i = [0:11]) {
            translate([width-pin_off_w, right_off_h + i*pin_off]) circle(d=pin_dia);
        }
        for (i = [0:15]) {
            translate([pin_off_w, left_off_h + i*pin_off]) circle(d=pin_dia);
        }
    }
}

module feather_3d() {
    linear_extrude(1.5) feather();
}

module key_plate(n,m) {
    pin_dia = 0.8;
    pin_off = 2.54;

    difference() {
        square([pin_off*n, pin_off*m]);
        for (i = [0:(n-1)]) {
            for (j = [1:(m-1)]) {
                translate([1.27 + pin_off*i, pin_off*j]) circle(d=pin_dia);
            }
        }
    }
}

module bot_key_plate_3d() {
    linear_extrude(1.5) key_plate(9,29);
}

module top_key_plate_3d() {
    linear_extrude(1.5) key_plate(7,22);
}

module key_3d() {
    translate([-7.2, -7.2, 0]) cube([14.4, 14.4, 10.7]);
    translate([-8.5, -8.5, 10.7-6]) cube([17, 17, 6]);
}

module bot_key_block() {
    feather_3d();
    translate([0, 16.51-2.54*2, pin_height+1.5]) union() {
        bot_key_plate_3d();
        translate([2.54*4.5, 2.54*4, 1.5]) key_3d();
        translate([2.54*4.5, 2.54*11, 1.5]) key_3d();
        translate([2.54*4.5, 2.54*18, 1.5]) key_3d();
        translate([2.54*4.5, 2.54*25, 1.5]) key_3d();
    }
}

module top_key_block() {
    union() {
        top_key_plate_3d();
        translate([2.54*3.5, 2.54*4, 1.5]) key_3d();
        translate([2.54*3.5, 2.54*11, 1.5]) key_3d();
        translate([2.54*3.5, 2.54*18, 1.5]) key_3d();
    }
}

module battery() {
    cube([6, 20, 80]);
}

//color("blue") bot_key_block();

// real stuff
thickness = 3;

feather_w = 22.86;
feather_h = 50.8;
feather_s_off = 2.54;
feather_s_dia = 3.0;

battery_w = 6;
battery_h = 60;

wall = 5;
gap = 0.5;
u_gap = 0.2;
usb_wall = 1.5;

top_holder = 3*thickness;

round_dia = 3;

screw_dia_d = 3.4;
screw_dia = 3;
screw_off = screw_dia;

usb_socket = 11.51;

keys_off = 16.51 - 2*2.54;

key_top_off_w = 2.3;
key_top_off_h = 1.1;

keys_h = 29*2.54;

main_comp_h = (keys_h + keys_off);

battery_slot_h = 10;
battery_off_h = 4;

base_w = wall + gap + feather_w + gap + wall + gap + battery_w + gap + wall;
base_h = usb_wall + u_gap + main_comp_h + gap + wall + top_holder;

connector_slot_w = 9;//17.54;

under_w = feather_w - screw_dia*2;
under_h = 35;

top_mount_w = base_w - screw_dia*4;

top_key_angle = -26;

case_thickness = 8*thickness;

strap_h = keys_h + keys_off + u_gap + usb_wall;

module feather_screws() {
    translate([feather_s_off, feather_s_off]) circle(d=feather_s_dia);
    translate([feather_s_off, feather_h-feather_s_off]) circle(d=feather_s_dia);
    translate([feather_w-feather_s_off, feather_s_off]) circle(d=feather_s_dia);
    translate([feather_w-feather_s_off, feather_h-feather_s_off]) circle(d=feather_s_dia);
}

top_slope = -7;
screw_off_left = 20;

module top_holder_0() {
    rotate([0,0,top_slope]) square([top_mount_w-layer1_w, 3*thickness]);
}

module top_holder_12() {
    rotate([0,0,top_slope]) union() {
        square([top_mount_w, thickness]);
        translate([0, thickness]) square([top_mount_w - thickness, 2*thickness]);
    }
}

module top_holder_34() {
    rotate([0,0,top_slope]) union() {
        square([top_mount_w, thickness]);
        translate([thickness, thickness]) square([top_mount_w - 2*thickness, 2*thickness]);
    }
}

module top_holder_56() {
    rotate([0,0,top_slope]) union() {
        square([top_mount_w, thickness]);
        translate([thickness, thickness]) square([top_mount_w - thickness, 2*thickness]);
    }
}

module top_holder_7() {
    rotate([0,0,top_slope]) translate([layer1_w, 0]) square([top_mount_w-layer1_w, 3*thickness]);
}

module base() {
    translate([round_dia, round_dia]) minkowski() {
        // FUG
        polygon([[0,0],
                 [base_w - 2*round_dia, 0],
                 [base_w - 2*round_dia, (base_h - 4.47) - 2*round_dia],
                 [screw_dia*2-3, base_h - 2*round_dia],
                 [0, base_h - 2*round_dia]]);
//        square([base_w - 2*round_dia , base_h - 2*round_dia]);
        circle(round_dia);
    }
}

module base_screws(dd=screw_dia) {
    translate([screw_off, screw_off]) circle(d=dd);
    translate([screw_off, base_h - screw_off]) circle(d=dd);
    translate([base_w - screw_off, screw_off]) circle(d=dd);
    translate([base_w - screw_off, base_h - screw_off_left]) circle(d=dd);
}

module case_0() {
    difference() {
        base();
        translate([wall + gap + feather_w/2 - usb_socket/2, strap_h - thickness]) square([usb_socket, thickness]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_0();
    }
}

module case_1() {
    difference() {
        union() {
            difference() {
                base();
                base_screws(screw_dia_d);
                translate([wall, usb_wall]) square([feather_w+2*gap, feather_h+gap+u_gap]);
                translate([wall + gap + feather_w + gap + wall , wall]) square([battery_w+2*gap, battery_h+2*gap]);
            }
            translate([wall + gap + feather_s_off, usb_wall + u_gap + feather_s_off]) square(2*screw_dia+gap/2,center=true);
            translate([wall + gap + feather_w - feather_s_off, usb_wall + u_gap + feather_s_off]) square(2*screw_dia+gap/2,center=true);
            translate([wall + gap + feather_s_off, usb_wall + u_gap + feather_h - feather_s_off]) square(2*screw_dia+gap/2,center=true);
            translate([wall + gap + feather_w - feather_s_off, usb_wall + u_gap + feather_h - feather_s_off]) square(2*screw_dia+gap/2,center=true);
        }
        translate([wall + gap + feather_s_off, usb_wall + u_gap + feather_s_off]) circle(d=feather_s_dia);
        translate([wall + gap + feather_w - feather_s_off, usb_wall + u_gap + feather_s_off]) circle(d=feather_s_dia);
        translate([wall + gap + feather_s_off, usb_wall + u_gap + feather_h - feather_s_off]) circle(d=feather_s_dia);
        translate([wall + gap + feather_w - feather_s_off, usb_wall + u_gap + feather_h - feather_s_off]) circle(d=feather_s_dia);
        translate([wall + gap + feather_w/2 - usb_socket/2, 0]) square([usb_socket, strap_h]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_12();
    }
}

module case_2() {
    difference() {
        base();
        base_screws();
        translate([wall, usb_wall]) square([feather_w+2*gap, feather_h+gap+u_gap]);
        translate([wall + gap + feather_w + gap + wall , wall]) square([battery_w+2*gap, battery_h+2*gap]);
        translate([wall + gap + feather_w/2 - usb_socket/2, 0]) square([usb_socket, usb_wall]);
        translate([wall + screw_dia, feather_h]) square([under_w, under_h]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_12();
    }
}

module case_3() {
    difference() {
        base();
        base_screws();
        translate([wall, usb_wall]) square([feather_w+2*gap, feather_h+gap+u_gap]);
        translate([wall + gap + feather_w + gap + wall, wall]) square([battery_w+2*gap, battery_h+2*gap]);
        translate([wall + gap + feather_w/2 - usb_socket/2, 0]) square([usb_socket, usb_wall]);
        translate([wall + gap + feather_w + gap, usb_wall + battery_off_h]) square([wall, battery_slot_h]);
        translate([wall + screw_dia, feather_h]) square([under_w, under_h]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_34();
    }
}

module case_4() {
    difference() {
        base();
        base_screws();
        translate([wall, wall]) square([feather_w+2*gap, feather_h+gap+u_gap - (wall - usb_wall)]);
        translate([wall + gap + feather_w + gap + wall, wall]) square([battery_w+2*gap, battery_h+2*gap]);
        translate([wall + gap + feather_w + gap, usb_wall + battery_off_h]) square([wall, battery_slot_h]);
        translate([wall + screw_dia, feather_h]) square([under_w, under_h]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_34();
    }
}

module case_5() {
    difference() {
        base();
        base_screws();
        translate([wall, wall]) square([feather_w+2*gap, feather_h+gap+u_gap - (wall - usb_wall)]);
        translate([wall + gap + feather_w + gap + wall , wall]) square([battery_w+2*gap, battery_h+2*gap]);
        translate([wall + feather_w/2 - connector_slot_w/2 + screw_dia*2, main_comp_h-wall]) square([connector_slot_w, wall*3]);
        translate([wall + screw_dia, feather_h]) square([under_w, under_h]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_56();
    }
}

module case_6() {
    difference() {
        base();
        base_screws();
        translate([wall, usb_wall+keys_off]) square([feather_w + 2*gap, keys_h + 2*gap]);
        translate([wall + gap + feather_w + gap + wall , wall]) square([battery_w+2*gap, battery_h+2*gap]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_56();
    }
}

module case_7() {
    difference() {
        base();
        base_screws();
        translate([wall+gap+key_top_off_w, usb_wall+u_gap+keys_off+key_top_off_h]) square([feather_w - 2*key_top_off_w, keys_h - 2*key_top_off_h]);
        //translate([wall + gap + feather_w + gap + wall , wall]) square([battery_w+2*gap, battery_h+2*gap]);
        translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall]) top_holder_7();
    }
}

module bot_3d() {
    translate([0,0,-2*thickness]) color("blue") linear_extrude(thickness) case_0();
    translate([0,0,-thickness]) color("red") linear_extrude(thickness) case_1();
    translate([0,0,0]) color("magenta") linear_extrude(thickness) case_2();
    translate([0,0,thickness]) color("magenta") linear_extrude(thickness) case_3();
    translate([0,0,2*thickness]) color("magenta") linear_extrude(thickness) case_4();
    translate([0,0,3*thickness]) color("green") linear_extrude(thickness) case_5();
    translate([0,0,4*thickness]) color("yellow") linear_extrude(thickness) case_6();
    translate([0,0,5*thickness]) color("yellow") linear_extrude(thickness) case_7();
    translate([wall + gap, usb_wall + u_gap]) #bot_key_block();
}

top_wall = 3;
top_plate_w = 7*2.54 + top_wall*2 + gap;
top_plate_h = 22*2.54 + top_wall*2 + gap;

top_screw_off_h = 6;
top_screw_off_w = 6.5;

layer1_w = 9.29;

module top_mount_sq() {
    square([top_mount_w, case_thickness-2*thickness]);
}

module top_mount_screws() {
    translate([top_screw_off_w, case_thickness - top_screw_off_h]) circle(d=screw_dia);
    translate([top_mount_w - top_screw_off_w, top_screw_off_h]) circle(d=screw_dia);
}

module top_mount_base() {
    difference() {
        union() {
            translate([0, thickness]) top_mount_sq();
            difference() {
                translate([top_mount_w/2,case_thickness/2]) rotate([0,0,top_key_angle]) translate([0, -2*2.54]) square([top_plate_w, top_plate_h], center=true);
                translate([-top_mount_w/2, thickness]) square([top_mount_w*2, case_thickness-2*thickness]);
            }
        }
        translate([top_mount_w-layer1_w, 0]) square([layer1_w, thickness]);
        translate([-layer1_w, 0]) square([layer1_w, thickness]);
        translate([0, case_thickness-thickness]) square([layer1_w, thickness]);
        translate([top_mount_w, case_thickness-thickness]) square([layer1_w, thickness]);
    }
}

module top_mount_connector_slot()
{
    translate([top_mount_w,0]) mirror() translate([wall + gap + feather_w/2 - connector_slot_w/2 - 2*screw_dia + screw_dia*2, 1.75*thickness]) square([connector_slot_w, thickness*1.5]);
}

module top_mount_case_0() {
    difference() {
        top_mount_base();
        top_mount_screws();
        top_mount_connector_slot();
    }
}

module top_mount_case_1() {
    difference() {
        top_mount_base();
        top_mount_screws();
        translate([top_mount_w/2,case_thickness/2]) rotate([0,0,top_key_angle]) translate([0, -2*2.54]) square([top_plate_w-top_wall*2, top_plate_h-top_wall*2], center=true);
        translate([0, case_thickness-thickness-4*thickness]) square([thickness, 4*thickness]);
        translate([top_mount_w-thickness, thickness]) square([thickness, 4*thickness]);
    }
}

module top_mount_case_2() {
    difference() {
        top_mount_base();
        top_mount_screws();
        translate([top_mount_w/2,case_thickness/2]) rotate([0,0,top_key_angle]) translate([0, -2*2.54]) square([top_plate_w-top_wall*2-2*key_top_off_h-gap, top_plate_h-top_wall*2-2*key_top_off_w-gap], center=true);
        translate([0, case_thickness-thickness-4*thickness]) square([thickness, 4*thickness]);
        translate([top_mount_w-thickness, thickness]) square([thickness, 4*thickness]);
    }
}

module top_3d() {
    translate([top_mount_w, 0]) mirror() {
        translate([0, 0, -3]) color("red") linear_extrude(thickness) translate([0, -case_thickness/2]) top_mount_case_0();
        translate([0, 0, 0]) color("blue") linear_extrude(thickness) translate([0, -case_thickness/2]) top_mount_case_1();
        translate([0, 0, 3]) color("green") linear_extrude(thickness) translate([0, -case_thickness/2]) top_mount_case_2();
        translate([0, 0, 1.5]) translate([top_mount_w/2,0]) rotate([0,0,top_key_angle]) translate([-2.54*3.5, -13*2.54]) #top_key_block();
    }
}

module 3d() {
    bot_3d();
    translate([screw_dia*2, usb_wall + u_gap + main_comp_h + gap + wall + thickness, thickness*2]) rotate([-90, 0, top_slope]) top_3d();
}

module 2d() {
    case_0();
    translate([50, 0]) case_1();
    translate([100, 0]) case_2();
    translate([150, 0]) case_3();
    translate([200, 0]) case_4();
    translate([250, 0]) case_5();
    translate([300, 0]) case_6();
    translate([350, 0]) case_7();

    translate([40, -50]) mirror() top_mount_case_0();
    translate([75, -50]) mirror() top_mount_case_1();
    translate([110, -50]) mirror() top_mount_case_2();

    translate([130, -20]) keycap(false);
    translate([130, -40]) keycap(true);
    translate([150, -20]) keycap(false);
    translate([150, -40]) keycap(true);
    translate([170, -20]) keycap(false);
    translate([170, -40]) keycap(true);
    translate([190, -20]) keycap(false);
    translate([190, -40]) keycap(true);
    translate([210, -20]) keycap(false);
    translate([210, -40]) keycap(true);
    translate([230, -20]) keycap(false);
    translate([230, -40]) keycap(true);
    translate([250, -20]) keycap(false);
    translate([250, -40]) keycap(true);
}

2d();
