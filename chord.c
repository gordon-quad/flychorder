/*
 * Copyright (C) 2018 Gordon Quad
 *
 * This file is part of FlyChorder.
 *
 * Bayan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>
#include <stdio.h>
#include <avr/pgmspace.h>

#include "ch.h"
#include "hal.h"

#include "chord.h"
#include "chord_layout.h"

#include "bluefruit.h"

#define TIMEOUT TIME_MS2I(5)

static msg_t btn_queue[BTN_COUNT];

MAILBOX_DECL(btn_events, btn_queue, BTN_COUNT);

static uint8_t layer_stack[LAYER_STACK_MAX];
static uint8_t layer_sp;

static uint8_t chord;

static uint8_t mods_locked;
static uint8_t mods_toggled;

static int16_t layer_toggled;

static bool releasing;

void chord_btn_process(uint8_t button, bool pressed);
void chord_process_chord(uint8_t chord);
void chord_execute_action(uint16_t action);
void chord_release_mods(void);
void chord_process_mod(uint8_t mod);
void chord_process_key(uint16_t action);
void chord_reset(void);

/*
 * Push layer on top of the layer stack
 * If stack is full - bottommost layer is removed
 */
static void layer_push(uint8_t layer)
{
    int i;
    if (layer_sp < LAYER_STACK_MAX-1)
    {
        layer_sp++;
        layer_stack[layer_sp] = layer;
    }
    else
    {
        for (i = 1; i < LAYER_STACK_MAX; i++)
            layer_stack[i - 1] = layer_stack[i];
        layer_stack[LAYER_STACK_MAX-1] = layer;
    }
}

/*
 * Pop layer from stack
 * If stack is empty after the pop - 0 layer pushed on top of the stack
 */
static void layer_pop(void)
{
    if (layer_sp > 0)
        layer_sp--;
    else
    {
        layer_sp = 0;
        layer_stack[layer_sp] = 0;
    }
}

static inline uint8_t layer_peek(void)
{
    return (layer_toggled >= 0) ? layer_toggled : layer_stack[layer_sp];
}

/*
 * Sets button state in chord
 */
uint8_t set_btn_state(uint8_t buttons, uint8_t button, bool pressed)
{
    if (pressed)
        return buttons | (1 << button);
    else
        return buttons & ~(1 << button);
}

void chord_btn_process(uint8_t button, bool pressed)
{
    if (pressed)
        releasing = false;
    else if (!releasing)
    {
        releasing = true;
        chord_process_chord(chord);
    }
    chord = set_btn_state(chord, button, pressed);
}

void chord_process_chord(uint8_t chord)
{
    int16_t old_toggled = layer_toggled;
    keymap_t *keymap = pgm_read_word(&chords[layer_peek()]);
    uint16_t action = pgm_read_word(&keymap[chord]);

    layer_toggled = -1;

    chord_execute_action(action);

    if (old_toggled >= 0)
    {
        chord_execute_action(layer_deactivation[old_toggled]);
        chord_execute_action(layer_activation[layer_peek()]);
    }
}

void chord_execute_action(uint16_t action)
{
    uint8_t cmd = action >> 8;
    uint8_t arg = action & 0xff;
    int i;
    uint16_t *macro;

    if (action == 0)
        return;

    switch(cmd)
    {
    case 0:
        chord_process_key(action);
        chord_release_mods();
        break;
    case RETURN_CMD:
        chord_execute_action(layer_deactivation[layer_peek()]);
        layer_pop();
        chord_execute_action(layer_activation[layer_peek()]);
        break;
    case SET_LAYER_CMD:
        chord_execute_action(layer_deactivation[layer_peek()]);
        layer_push(arg);
        chord_execute_action(layer_activation[layer_peek()]);
        break;
    case TOGGLE_LAYER_CMD:
        chord_execute_action(layer_deactivation[layer_peek()]);
        layer_toggled = arg;
        chord_execute_action(layer_activation[layer_peek()]);
        break;
    case RESET_CMD:
        chord_reset();
        break;
    case MRESET_CMD:
        palSetPadMode(IOPORT3, PC6, PAL_MODE_OUTPUT_PUSHPULL);
        palClearPad(IOPORT3, PC6);
        break;
    case MACRO_CMD:
        macro = (uint16_t *)pgm_read_word(&macros[arg]);
        for (i = 0; i < MACRO_LEN; ++i)
            chord_execute_action(pgm_read_word(&macro[i]));
        break;
    case MOD_CMD:
        chord_process_mod(arg);
        break;
    }
}

void chord_release_mods(void)
{
    int i;
    for (i = 0; i < 8; ++i)
        if ((mods_toggled & (1 << i)) && !(mods_locked & (1 << i)))
            mods_toggled &= ~(1 << i);
}

void chord_process_mod(uint8_t mod)
{
    if (mods_locked & mod)
    {
        mods_locked &= ~mod;
        mods_toggled &= ~mod;
    }
    else if (mods_toggled & mod)
        mods_locked |= mod;
    else
        mods_toggled |= mod;
}

void chord_process_key(uint16_t action)
{
    bluefruit_press_key(action & 0xff, mods_toggled);
}

void chord_reset(void)
{
    layer_sp = 0;
    layer_stack[0] = 0;
    mods_toggled = 0;
    mods_locked = 0;
    releasing = true;
    layer_toggled = -1;
}

/*
 * Chord recognition worker thread
 */
static thread_t *chord_thread;
static THD_WORKING_AREA(waChord, 256);
static THD_FUNCTION(ChordThread, arg)
{
    (void)arg;
    msg_t btn;
    
    for(;;)
    {
        if ((chMBFetchTimeout(&btn_events, &btn, TIMEOUT) == MSG_TIMEOUT) &&
            chThdShouldTerminateX())
        {
            while(chMBFetchTimeout(&btn_events, &btn, TIME_IMMEDIATE) != MSG_TIMEOUT);
            break;
        }

        if (btn > 255)
            chord_btn_process(btn - 0x100, false);
        else
            chord_btn_process(btn, true);
        if (chMBFetchTimeout(&btn_events, &btn, TIME_IMMEDIATE) == MSG_OK)
            chMBPostAheadTimeout(&btn_events, btn, TIME_IMMEDIATE);

    }
}

void chord_init(void)
{
    chord_reset();
    chord = 0;
}

void chord_start(void)
{
    chord_thread = chThdCreateStatic(waChord, sizeof(waChord), NORMALPRIO, ChordThread, NULL);
}

void chord_terminate(void)
{
    chThdTerminate(chord_thread);
    chThdWait(chord_thread);
}
